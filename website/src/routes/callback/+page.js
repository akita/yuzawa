import { createAuthClient } from '$lib/auth.ts';
import { logInUser } from '$lib/auth.js';

let user = null;
let isAuthenticated = false;
let auth0 = null;

export let ssr = false;

async function init() {
	auth0 = await createAuthClient();

	if (
		location.search.includes('state=') &&
		(location.search.includes('code=') || location.search.includes('error='))
	) {
		console.log('hit if-statement');
		await auth0?.handleRedirectCallback();
		window.history.replaceState({}, document.title, '/');
	}

	isAuthenticated = await auth0.isAuthenticated();
	user = await auth0.getUser();

	if (user != undefined) {
		await logInUser(user.email);
	}
}

export async function load() {
	await init();
}
