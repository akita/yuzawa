import jwt, { type Secret } from 'jsonwebtoken';
import dotenv from 'dotenv';
dotenv.config();

export const POST = async ({ params, request }: { params: Object, request: Request }) => {
    var b = await request.json()
    var jwt_secret: Secret = process.env.JWT_SECRET || "";

    try {
        let u: Object = await jwt.verify(b.token, jwt_secret)
        return new Response(JSON.stringify({
            status: 200,
            body: u
        }))
    } catch (e) {
        return new Response(JSON.stringify({
            status: 200,
            body: e
        }))
    }
}