import { connectToDatabase } from '$lib/db.js';

export const POST = async ({ params, request }: { params: Object, request: Request }) => {
    var b = await request.json()
    // console.log(b)
    let dbTest = await connectToDatabase();
    // console.log(dbTest.db)

    var c = await dbTest.db.collection(b.collection).insertOne(b.items)

    let response: APIResponse = {
        status: c ? true : false,
        msg: c ? "Values saved successfully!" : "Database had an error/returned nothing.",
        data: c
    }
    console.log(response)
    return new Response(JSON.stringify({
        status: 200,
        body: response
    }))
}