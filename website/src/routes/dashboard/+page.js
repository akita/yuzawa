import { browser } from '$app/environment';
import {
    DB,
    UserLoginFlow,
} from '/src/lib/utils';

export const ssr = false;

export async function load() {
    if (browser) {
        const t = await UserLoginFlow();

        let usersEnvironments = await DB.findMultipleItem('environments', {
            $or: [{ owner: t.id }, { viewer: t.id }]
        });

        let usersConfigs = await DB.findMultipleItem('configs', {
            $or: [{ owner: t.id }, { viewer: t.id }]
        });

        console.log(browser);

        return {
            status: 200,
            environments: usersEnvironments.data,
            configs: usersConfigs.data,
            userInformation: {
                id: t.id
            }
        }
    }
}