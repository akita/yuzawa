import { browser } from '$app/environment';
import { UserLoginFlow } from '$lib/utils.ts';

export const ssr = false;

// Redirect to login if not logged in
export async function load() {
    let userToken;

    if (browser) {
        console.log(userToken);
        userToken = await UserLoginFlow();
        return {
            status: 200,
            props: {
                userToken
            }
        };
    }
    return {
        status:200
    }
}