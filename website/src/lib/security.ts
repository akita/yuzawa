import CryptoJS from 'crypto-js';
import { PUBLIC_API_SECRET } from '$env/static/public';

const personalString: string = 'hi yuzawa :)'

export const createAuthToken = () => {
    return CryptoJS.AES.encrypt(Date.now().toString() + " // " + personalString, PUBLIC_API_SECRET).toString();
}

export const verifyAuthToken = (token: String) => {
    let decrypted = CryptoJS.AES.decrypt(token, PUBLIC_API_SECRET).toString(CryptoJS.enc.Utf8);
    let time = Date.now();

    if (time - parseInt(decrypted.split(" // ")[0]) < 1100 && decrypted.split(" // ")[1] === personalString) {
        return true;
    }
}