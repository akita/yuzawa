import { DB, JWT } from '$lib/utils.ts';
import { user } from '/src/stores/store.js';

export async function logInUser(email) {
    let checkIfUserExists = await DB.findOneItem('users', { email });

    if (!checkIfUserExists.status) {
        let insertNewUser = await DB.insertItem('users', {
            email,
            createdAt: Date.now()
        });

        let userJWT = await JWT.sign({
            email,
            id: insertNewUser.data.insertedId
        });

        user.set(userJWT);

        window.location.href = '/create-profile';
        return;
    } else {
        let userJWT = await JWT.sign({
            email,
            id: checkIfUserExists.data._id
        });

        user.set(userJWT);
    }

    window.location.href = '/dashboard';
}