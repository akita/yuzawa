import { writable } from 'svelte-local-storage-store'

export const user = writable('jwt', null)
export const isValid = (ts) => {
    return !(Date.now() - 432000000 > ts)
}