/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/**/*.{html,js,svelte,ts}'
  ],
  theme: {
    extend: {
      colors: {
        'primary': '#3db2d1',
        'secondary': '#f1843f',
        'success': '#84bd6d',
        'warning': '#ffb30f',
        'danger': '#ed3705',
      }
    },
    plugins: [
      require('daisyui')
    ]
  }
}