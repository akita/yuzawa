package main

import (
	"fmt"
	"net/http"
	"net/url"
	"os/exec"

	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()
	router.GET("/", pingServer)
	router.GET("/test", executeTestCommand)

	router.POST("/version", getVersionList)

	router.Run(":8080")
}

// Ping server on root endpoint
func pingServer(c *gin.Context) {
	c.IndentedJSON(http.StatusOK, gin.H{"message": "Pong! Server is alive."})
}

/*
Execute "echo 'Hello World!'"
Expected output: 'Hello World!'\n
*/
func executeTestCommand(c *gin.Context) {
	cmd := exec.Command("echo", "'Hello World!'")

	output, err := cmd.Output()

	if err != nil {
		fmt.Println(err)
	}

	c.IndentedJSON(http.StatusOK, gin.H{
		"executedCommand": "echo 'Hello World!'",
		"returnedMessage": string(output),
	})
}

/* --- End Defaults --- */

func getVersionList(c *gin.Context) {
	var urlToCheck CheckVersion

	if err := c.ShouldBindJSON(&urlToCheck); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	_, err := url.ParseRequestURI(urlToCheck.URL)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	cmd := exec.Command("git", "ls-remote", "--tags", urlToCheck.URL, "|", "sed", "'s/.*\\///; s/\\^{}//'")

	output, err := cmd.Output()
	if err != nil {
		fmt.Println(err)
	}

	c.IndentedJSON(http.StatusOK, gin.H{
		"versions": string(output),
	})
}
