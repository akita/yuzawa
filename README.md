# Yuzawa

[![Netlify Status](https://api.netlify.com/api/v1/badges/4db7d1e8-3365-4bc7-a150-b3c434c701fb/deploy-status)](https://app.netlify.com/sites/chipdb/deploys)

Yuzawa is a Low/No-Code Simulation-as-a-Service that allows for running hardware configurations in the cloud. 

## Standards

Yuzawa is built in SvelteKit (which has recently reached v1.0.0). Yuzawa uses some package standards that should be used to maintain uniformity in the project:

- **Typescript**: TypeScript should *only* be used for helper files (`db.ts`, `utils.ts`) or API endpoints. Otherwise, there will be problems in mixing JS and TS. 
- **Tailwind**: For styling, we are using TailwindCSS ([https://tailwindcss.com/](https://tailwindcss.com/)). Please try to use Tailwind as much as possible for styling, unless there truly is no way to use it for a specific design element. 
- **Toasts**: For notifying the user, we are using a custom toasts plugin with a custom API wrapper. See `lib > utils.ts`.
- **DB Access**: To access the DB, we are using MongoDB as the structure, and a custom API wrapper to access it. See `lib > utils.ts`.

Of course, as we are working in SvelteKit, maintain the coding styles as mentioned on the SvelteKit Docs website. 

## Development

To get started on developing, clone this repository:

```bash
git clone https://gitlab.com/akita/yuzawa.git
cd ./yuzawa
```

Then, install all dependencies. This only needs to be done once! Make sure you have NodeJS installed on your machine to do this:

```bash
npm i
```

Lastly, start the development server. It will allow you to access Yuzawa on `localhost:5173`:

```bash
npm run dev
```

### Branches

To checkout into a specific branch on your machine, you can run:

```bash
git checkout <name of branch>
```

Or you can make a new branch by adding `-b` to the command:

```bash
git checkout -b <name of new branch>
```

Remember to push often and make new branches for different features.

## Deployment

This project automatically deploys with Netlify on every MR and every commit to the main branch. 

<a href="https://www.netlify.com"> <img src="https://www.netlify.com/v3/img/components/netlify-color-bg.svg" alt="Deploys by Netlify" /> </a>

For local deployment, run the following commands in the `website` directory.

```bash
npm install
npm run dev
```
